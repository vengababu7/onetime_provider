//
//  HelpViewController.h
//  privMD
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface iServeHelpController : UIViewController<UIScrollViewDelegate>

- (IBAction)signInButtonClicked:(id)sender;
- (IBAction)registerButtonClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *backgroundView;
@property (strong, nonatomic) IBOutlet UIView *bottomContainerView;

@end
