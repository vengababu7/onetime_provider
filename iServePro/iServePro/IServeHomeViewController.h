//
//  IServeHomeViewController.h
//  iServePro
//
//  Created by Rahul Sharma on 25/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeCell.h"

@interface IServeHomeViewController : UIViewController<HomeCellDelegate>
{
    
}
@property(nonatomic,assign)DriverStatus driverStatus;

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property(assign,nonatomic)int counter;
@property (strong, nonatomic) NSDictionary *dictAppointments;
@property (strong, nonatomic) NSString *status;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightOfContent;

@end
