//
//  NewBookingController.m
//  iServePro
//
//  Created by Rahul Sharma on 03/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "NewBookingController.h"
#import <AVFoundation/AVFoundation.h>
#import "ProgressIndicator.h"
#import "PICircularProgressView.h"
#import "ChatSIOClient.h"
#import "ChatSocketIOClient.h"

@interface NewBookingController ()
{
    ChatSocketIOClient *socket1;
    ChatSIOClient *soketIOClient;
}
@property (strong, nonatomic) IBOutlet GMSMapView *mapView_;

@property (strong, nonatomic)  AVAudioPlayer *audioPlayer;
@property (assign, nonatomic) float expireTime;
@property (strong, nonatomic) NSTimer *timerProgress;
@property (strong, nonatomic) NSMutableArray *array;


@end

@implementation NewBookingController
- (void)viewDidLoad {
    
    [super viewDidLoad];
    socket1  =[ChatSocketIOClient sharedInstance];
    soketIOClient =[ChatSIOClient sharedInstance];
    
    if (_detailsDict.count) {
        _array=[_detailsDict mutableCopy];
    }
    self.progressView.backgroundColor = [UIColor colorWithWhite:0.333 alpha:0.730];
    self.progressView.layer.cornerRadius = 90;
    self.progressView.layer.masksToBounds = YES;
    
    NSString *pewPewPath = [[NSBundle mainBundle]
                            pathForResource:@"taxina"
                            ofType:@"wav"];
    NSURL *pewPewURL = [NSURL fileURLWithPath:pewPewPath];
    if (!_audioPlayer) {
        _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:pewPewURL error:nil];
    }
    _audioPlayer.numberOfLoops = 0;
    [_audioPlayer prepareToPlay];
    [_audioPlayer play];
    NSString *staticMapUrl = [NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?markers=color:red|%f,%f&%@&sensor=true",[_array[0][@"lat"] doubleValue], [_array[0][@"long"] doubleValue],@"zoom=14&size=180x180"];
    NSURL *mapUrl = [NSURL URLWithString:[staticMapUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    UIImage *image = [UIImage imageWithData: [NSData dataWithContentsOfURL:mapUrl]];
    _mapImage.image=image;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"acceptBooking" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"rejectBooking" object:nil];
    
    [self startCoundownTimer];
    [self UpdateCustomerInfo];
    _isTimeOver=YES;
}
//- (void)receiveNotification:(NSNotification *)notification{
- (void)receiveNotification:(NSDictionary *)dict{
    // NSDictionary *dict = notification.userInfo;;
    _array=[dict[@"aps"] mutableCopy];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"bookingStatusResponse"] integerValue]==2) {
        [self acceptOrder:nil];
    }else{
        [self rejectOrder:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    [_timerProgress invalidate];
    _timerProgress = nil;
}

-(void)UpdateCustomerInfo
{
    _custLoc1.text =_array[0][@"add"];
    _requestType.text=[NSString stringWithFormat:@"New %@ Request",_array[0][@"catname"]];
    
}


- (IBAction)acceptOrder:(id)sender {
    _isTimeOver=NO;
    [self respondToNewBookingFor:2];
}

- (IBAction)rejectOrder:(id)sender {
    _isTimeOver=NO;
    [self respondToNewBookingFor:3];
}



-(void)respondToNewBookingFor:(int)bookingAction{
    
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:@"Loading.."];
    NSDictionary *queryParams;
    queryParams =@{
                   KDAcheckUserSessionToken     :[[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                   kSMPCommonDevideId           :[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                   kSMPRespondPassengerEmail    :flStrForObj(_array[0][@"email"]),
                   kSMPRespondBookingDateTime   :flStrForObj(_array[0][@"dt"]),
                   kSMPRespondResponse          :[NSString stringWithFormat:@"%d",bookingAction],
                   kSMPRespondBookingType       :constkNotificationTypeBookingType,
                   kSMPCommonUpDateTime         : [Helper getCurrentDateTime],
                   @"ent_bid"                   :flStrForObj(_array[0][@"bid"]),
                   @"ent_proid"                 :[[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"]
                   };
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:MethodRespondToAppointMent
                                    paramas:queryParams
                               onComplition:^(BOOL success, NSDictionary *response){                                                                      ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                                   [pi hideProgressIndicator];
                                   
                                   if (success) { //handle success response
                                       
                                       if ([response[@"errFlag"] integerValue] == 0)
                                       {
                                           
                                           if (bookingAction == 2)
                                           {
                                               //                                               [[NSNotificationCenter defaultCenter] postNotificationName:@"InBooking" object:nil];
                                               [self emitTheBookingACk:bookingAction];
                                               dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                                   [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissNewBookingVC" object:response];
                                               });

                                           }
                                           else if (bookingAction == 3)
                                           {
                                               //                                               [[NSNotificationCenter defaultCenter] postNotificationName:@"InBooking" object:nil];
                                               [self emitTheBookingACk:bookingAction];
                                           }
                                       }
                                       else if ([response[@"errFlag"] integerValue] == 1) {
                                           [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Error",@"Error") Message:response[@"errMsg"]];
                                           [self dismissViewControllerAnimated:YES completion:nil];
                                       }else{
                                           [self dismissViewControllerAnimated:YES completion:nil];
                                       }
                                   }
                                   else{
                                   }
                               }];
}

-(void)emitTheBookingACk:(int)bstatus
{
    ChatSIOClient *socket = [ChatSIOClient sharedInstance];
    NSDictionary *message=@{
                            @"bid":_array[0][@"bid"],
                            @"bstatus":[NSNumber numberWithInt:bstatus],
                            @"cid":_array[0][@"cid"],
                            @"proid":[[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"],
                            @"dt":[Helper getCurrentDateTime]
                            };
    
    
    if (!soketIOClient.isSocketConnected) {
        [socket1 socketIOSetup];
        double delayInSeconds = 2.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [socket publishToChannel:@"LiveBookingAck" message:message];
        });
    }else{
        [socket publishToChannel:@"LiveBookingAck" message:message];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)startCoundownTimer{
    
    self.progressView.thicknessRatio=0.2;
    self.progressView.progress = 1;
    self.progressView.showText =YES;
    self.progressView.roundedHead = YES;
    [self.progressView setProgressText:[NSString stringWithFormat:@"%.0d\nseconds",30]];
    _expireTime = 30;
    if (![_timerProgress isValid]) {
        _timerProgress = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
    }
}

- (void)timerTick
{
    float Newvalue1 = _expireTime/30 - (0.033333333);
    float Newvalue = _expireTime - 1;
    [_progressView setProgressText:[NSString stringWithFormat:@"%.0f\nseconds", Newvalue]];
    _progressView.progress = Newvalue1;
    _expireTime = Newvalue;
    if (Newvalue <= 0.0) {
        if (_isTimeOver) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"InBooking" object:nil];
            
        }
        [_timerProgress invalidate];
        [_progressView removeFromSuperview];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
