//
//  AppointmentDetailController.m
//  iServePro
//
//  Created by Rahul Sharma on 03/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "AppointmentDetailController.h"
#import "CustomSliderView.h"
#import "iServeSplashController.h"
#import "BookingViewController.h"
#import "ChatSocketIOClient.h"
#import "AmazonTransfer.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CustomerRatingPOPUP.h"
#import "LocationTracker.h"
#import "MTGoogleMapCustomURLInteraction.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "ImageViewCollection.h"
#import "PendingDetailsCollectionCell.h"
#import "ServicesTableViewCell.h"



#define BTNONTHEWAY 300

@interface AppointmentDetailController()<CustomSliderViewDelegate,UIActionSheetDelegate,ratingPopDelegate,MFMessageComposeViewControllerDelegate>
{
    CustomerRatingPOPUP *cancelReasons;
    GMSGeocoder *geocoder_;
    ImageViewCollection *collectionImages;
}
enum iServeProStatus{
    ON_THE_WAY = 5,
    AM_ARRIVED = 6,
    INVOICE = 7
};
@property(nonatomic,assign)BOOL isUpdatedLocation;
@property (strong, nonatomic) CustomSliderView *customSliderView;
@property(nonatomic,assign)BOOL isDriverArrived;


@end

@implementation AppointmentDetailController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.noJobPhotos setHidden:YES];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    NSString *title =[NSString stringWithFormat:@"JOB ID:%@",_dictAppointmentDetails[@"bid"]];
    self.title =title;
    [self addCustomSlider];
    _topView.layer.masksToBounds = NO;
    _topView.layer.shadowColor = [UIColor colorWithWhite:0.839 alpha:0.880].CGColor;
    _topView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
    _topView.layer.shadowOpacity = 0.5f;
    _topView.layer.shadowRadius = 2.0f;
    [self createNavLeftButton];
    [self updateCustomerData];
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain
                                                                          target:self action:@selector(popupCancelBooking)];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
    
    if ( [_dictAppointmentDetails[@"services"] count] == 0) {
        self.heightTable.constant = 100;
        UILabel *messageLbl = [[UILabel alloc] initWithFrame:CGRectMake(self.serviceTable.bounds.size.width/2,self.serviceTable.bounds.size.height/2 , 120, 21)];
        messageLbl.text = @"No Services found";
        messageLbl.textAlignment = NSTextAlignmentCenter;
        self.serviceTable.backgroundView = messageLbl;
    }else{
        [self.serviceTable reloadData];
        self.heightTable.constant = self.serviceTable.contentSize.height;
        [self.view layoutIfNeeded];
    }

    
}
-(void)popupCancelBooking{
    cancelReasons = [[CustomerRatingPOPUP alloc]init];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [cancelReasons onWindow:window];
    cancelReasons.delegate = self;
    
}

-(void)popUpRatingOFfDismiss:(NSString *)Reason{
      [self cancelAppointment:Reason];

    
}

-(void)viewWillAppear:(BOOL)animated{
    //  [self disableSlidePanGestureForLeftMenu];
}

-(void)addCustomSlider {
    
    _customSliderView = [[[NSBundle mainBundle] loadNibNamed:@"NewSliderView" owner:self options:nil] lastObject];
    _customSliderView.delegate = self;
    _customSliderView.sliderTitle.text = @"ON THE WAY";
    CGRect frame = _customSliderView.frame;
    frame.size.width = _sliderView.frame.size.width;
    frame.size.height = _sliderView.frame.size.height;
    _customSliderView.frame = frame;
    [_sliderView addSubview:_customSliderView];
    
}
-(void) createNavLeftButton
{
    UIImage *buttonImageNavNormal =[UIImage imageNamed:@"bnt_bck_normal"];
    UIImage *buttonImageNavHigh = [UIImage imageNamed:@"bnt_bck_pressed"];
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavHigh.size.width,buttonImageNavHigh.size.height)];
    [cancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImageNavNormal.size.width,buttonImageNavNormal.size.height)];
    [cancelButton setBackgroundImage:buttonImageNavHigh forState:UIControlStateHighlighted];
    [cancelButton setImage:buttonImageNavNormal forState:UIControlStateNormal];
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc]
                                               initWithCustomView:cancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -5;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}
-(void)backButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)updateCustomerData{
    _custName.text =_dictAppointmentDetails[@"fname"];
    _address1.text=_dictAppointmentDetails[@"addrLine1"];
    _address2.text =_dictAppointmentDetails[@"addrLine2"];
    if ([_dictAppointmentDetails[@"customer_notes"]  isEqual:@""]) {
        _additionalNotes.text=@"No Notes Provided By The Customer";
        
    }else{
        _additionalNotes.text=_dictAppointmentDetails[@"customer_notes"];
    }
    
}
#pragma mark - Custom Slider Delegate

-(void)sliderAction
{
    [self onTHEWAYButtonTapped];
    
}
-(void)onTHEWAYButtonTapped
{
    int iServeProStatus = 0;
    
    self.navigationItem.leftBarButtonItem.enabled = NO;
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    
    switch ([self.dictAppointmentDetails[@"status"] intValue]) {
            
        case 2:{
            iServeProStatus = ON_THE_WAY;
            
            break;
        }
        default:
            break;
    }
    
    
    
    NSDictionary *parameters = @{
                                 kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                 kSMPCommonDevideId       :[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                 @"ent_appnt_dt"          :self.dictAppointmentDetails[@"apntDt"],
                                 KSMPPatientEmail         :self.dictAppointmentDetails[@"email"],
                                 kSMPRespondResponse      :[NSString stringWithFormat:@"%d",iServeProStatus],
                                 @"ent_date_time"         :[Helper getCurrentDateTime],
                                 @"ent_bid"               :self.dictAppointmentDetails[@"bid"]
                                 };
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    
    [handler composeRequestWithMethod:MethodupdateApptStatus
                              paramas:parameters
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             [_customSliderView sliderImageOrigin];
                             if (succeeded) { //handle success response
                                 
                                 if ([response[@"errFlag"] isEqualToString:@"1"]) {
                                     [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:response[@"errMsg"]];
                                 }else{
                                     [self getONTHEWAYResponse:response];
                                     [self emitTheBookingACk:iServeProStatus];
                                     [self performSegueWithIdentifier:@"toMapVC" sender:_dictAppointmentDetails];
                                 }
                             }
                             else
                             {
                                 NSLog(@"Error");
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 self.navigationItem.leftBarButtonItem.enabled = YES;
                                 
                             }
                             
                         }];
}
-(void)emitTheBookingACk:(int)bstatus
{
    ChatSIOClient *socket = [ChatSIOClient sharedInstance];
    NSDictionary *message=@{
                            @"bid":_dictAppointmentDetails[@"bid"],
                            @"bstatus":[NSNumber numberWithInt:bstatus],
                            @"cid":_dictAppointmentDetails[@"cid"],
                            @"proid":[[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"],
                            @"dt":[Helper getCurrentDateTime]
                            };
    [socket publishToChannel:@"LiveBookingAck" message:message];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if([[segue identifier] isEqualToString:@"toMapVC"])
    {
        BookingViewController *details =[segue destinationViewController];
        details.dictBookingDetails =sender;
    }
    return;
}

-(void)getONTHEWAYResponse:(NSDictionary *)response
{
    
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",@"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",@"OK") otherButtonTitles:nil];
        [alertView show];
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"Error",@"Error") Message:[response objectForKey:@"Error"]];
        
    }else if ([[response objectForKey:@"errFlag"] intValue] == 1 && ([[response objectForKey:@"errNum"] intValue] == 6 || [[response objectForKey:@"errNum"] intValue] == 7))
    {   // session token expire
        
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [self userSessionTokenExpire];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Session Expired",@"Session Expired") message:NSLocalizedString(@"Your session with Melikey is expired. Please Login again.",@"Your session with Melikey is expired. Please Login again.") delegate:nil cancelButtonTitle:NSLocalizedString(@"Ok",@"Ok") otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else
    {
        NSDictionary *dictResponse  = response;
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
            UIButton *button = (UIButton *)[self.view viewWithTag:BTNONTHEWAY];
            
            
            
            if ([self.dictAppointmentDetails[@"status"] intValue] == 6) {
                [self buttonBackTapped];
            }else if([self.dictAppointmentDetails[@"status"] intValue] == 2) { //on the way
                [self.dictAppointmentDetails setValue:@"5" forKey:@"status"];
                
                /* change navigation rightbar button title */
                
                self.navigationItem.rightBarButtonItem = nil;
                
                
                
                
                
                _isUpdatedLocation = NO;
                _isDriverArrived = YES;
                
            }else if([self.dictAppointmentDetails[@"status"] intValue] == 5) { //Arrived
                [self.dictAppointmentDetails setValue:@"6" forKey:@"status"];
                self.navigationItem.rightBarButtonItem = nil;
                [button setTitle:NSLocalizedString(@"RAISE INVOICE",@"RAISE INVOICE") forState:UIControlStateNormal];
            }
        }
        else
        {
            
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
}

-(void)userSessionTokenExpire{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}

- (void)buttonBackTapped{
    
    [self.navigationController popViewControllerAnimated:YES];
}



/**
 *  Booking Cancel Method
 *
 *  @param reason "ISERVEPRO" can be before he starts
 */

- (void)cancelAppointment:(NSString *)reason{
    
    self.navigationItem.leftBarButtonItem.enabled = NO;
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
    
    NSDictionary *parameters = @{
                                 kSMPcheckUserSessionToken: flStrForObj([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken]),
                                 kSMPCommonDevideId       :flStrForObj([[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey]),
                                 //                                 @"ent_appnt_dt"          :flStrForObj(self.dictAppointmentDetails[@"apntDt"]),
                                 //                                 @"ent_email"             :flStrForObj(self.dictAppointmentDetails[@"email"]),
                                 @"ent_reason"            :flStrForObj(reason),
                                 @"ent_date_time"         :[Helper getCurrentDateTime],
                                 @"ent_bid":_dictAppointmentDetails[@"bid"]
                                 };
    
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:MethodabortAppointment
                              paramas:parameters
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             
                             if (succeeded) {
                                 [self emitCanceltoSocket:reason];
                                 [self backButtonPressed];
                                 self.navigationItem.leftBarButtonItem.enabled = YES;
                             }
                             else{
                                 
                                 NSLog(@"Error");
                                 self.navigationItem.leftBarButtonItem.enabled = YES;
                             }
                         }];
}

-(void)emitCanceltoSocket:(NSString *)reason
{
    ChatSIOClient *socket = [ChatSIOClient sharedInstance];
    NSDictionary *message=@{
                            @"bid":_dictAppointmentDetails[@"bid"],
                            @"bstatus":[NSNumber numberWithInt:10],
                            @"cid":_dictAppointmentDetails[@"cid"],
                            @"proid":[[NSUserDefaults standardUserDefaults] objectForKey:@"ProviderId"],
                            @"dt":[Helper getCurrentDateTime],
                            @"ent_pro_reason":reason
                            };
    [socket publishToChannel:@"LiveBookingAck" message:message];
    
}

#pragma uicollectionView delegates

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([_dictAppointmentDetails[@"job_imgs"] integerValue]==0) {
        [self.noJobPhotos setHidden:NO];
    }
    return [_dictAppointmentDetails[@"job_imgs"] integerValue];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier =@"customerImages";
    PendingDetailsCollectionCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSString *strImageUrl = [NSString stringWithFormat:@"https://s3.amazonaws.com/%@/JobImages/%@_%ld.png",Bucket,_dictAppointmentDetails[@"bid"],(long)indexPath.row];
    
    [cell.activityIndicator startAnimating];
    
    [cell.appointmentImage sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                              placeholderImage:[UIImage imageNamed:@"user_image_default"]
                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                         
                                         [cell.activityIndicator stopAnimating];
                                         
                                     }];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    collectionImages= [ImageViewCollection sharedInstance];
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [[NSUserDefaults standardUserDefaults] setObject:_dictAppointmentDetails[@"bid"] forKey:@"BID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSInteger profileTag =0;
    [collectionImages showPopUpWithDictionary:window jobImages:[_dictAppointmentDetails[@"job_imgs"] integerValue] index:indexPath tag:profileTag];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(66, 66);
}


- (IBAction)callAction:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Call", @"Call") message:[NSString stringWithFormat:@"%@ ",_dictAppointmentDetails[@"phone"]] delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel") otherButtonTitles:NSLocalizedString(@"Call", @"Call"), nil];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertVie didDismissWithButtonIndex:(NSInteger) buttonIndex
{
    if (buttonIndex == 0)
    {
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1)
    {
        NSString *phoneNumber = _dictAppointmentDetails[@"phone"];
        NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", phoneNumber];
        NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
}

- (IBAction)messageAction:(id)sender {
    if ([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController *message = [[MFMessageComposeViewController alloc] init];
        message.messageComposeDelegate = self;
        message.recipients=@[_dictAppointmentDetails[@"phone"]];
        [[message navigationBar] setTintColor:[UIColor blackColor]];
        message.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
        
        [message setBody:@""];
        [self presentViewController:message animated:YES completion:nil];
    }
}

- (IBAction)locateAction:(id)sender {
    LocationTracker *locaitontraker = [LocationTracker sharedInstance];
    float latitude = locaitontraker.lastLocaiton.coordinate.latitude;
    float longitude = locaitontraker.lastLocaiton.coordinate.longitude;
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
    [self getAddress:position];
    
}
#pragma mark UIAddress Formatter

- (void)getAddress:(CLLocationCoordinate2D)coordinate {
    
    if (!geocoder_) {
        geocoder_ = [[GMSGeocoder alloc] init];
    }
    
    
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response,
                                          NSError *error) {
        if (response && response.firstResult) {
            
            GMSAddress *address = response.firstResult;
            NSString *start = [address.lines componentsJoinedByString:@","];
            
            [self performSelectorOnMainThread:@selector(openDirection:) withObject:start waitUntilDone:YES];
            
        }else {
            // NSLog(@"Could not reverse geocode point (%f,%f): %@",
            //coordinate.latitude, coordinate.longitude, error);
        }
    };
    
    [geocoder_ reverseGeocodeCoordinate:coordinate
                      completionHandler:handler];
    
}

- (void)openDirection:(NSString *)startLocation{
    
    NSString *destination =_dictAppointmentDetails[@"addrLine1"];
    
    [MTGoogleMapCustomURLInteraction showDirections:@{DirectionsStartAddress: startLocation,
                                                      DirectionsEndAddress: destination,
                                                      DirectionsDirectionMode: @"Driving",
                                                      ShowMapKeyZoom: @"7",
                                                      ShowMapKeyViews: @"Satellite",
                                                      ShowMapKeyMapMode: @"standard"
                                                      }
                                      allowCallback:YES];
}
-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"You cancelled sending message");
            break;
        case MessageComposeResultFailed:
            NSLog(@"Message failed");
            break;
        case MessageComposeResultSent:
            NSLog(@"Message sent");
            break;
            
        default:
            NSLog(@"An error occurred while composing this message");
            break;
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
}
#pragma mark UITableview DataSource -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return  [_dictAppointmentDetails[@"services"] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"services";
    ServicesTableViewCell *cell;
    if (!cell) {
        cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    
    cell.serviceName.text = _dictAppointmentDetails[@"services"][indexPath.row][@"sname"];
    
    NSNumber *ServicePrice =[NSNumber numberWithInteger:[ _dictAppointmentDetails[@"services"][indexPath.row][@"sprice"] integerValue]];
    NSString *price = [formatter stringFromNumber:ServicePrice];
    cell.serviceVal.text = price;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView * labelview  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 25)];
    UILabel *labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(12, 3, 320, 22)];
    
    [labelview addSubview:labelHeader];
    
    if (section == 0) {
        [Helper setToLabel:labelHeader Text:@"SELECTED SERVICES" WithFont:@"opensans" FSize:11 Color:UIColorFromRGB(0xaaaaaa)];
        labelview.backgroundColor = UIColorFromRGB(0xf8f8f8);
    }
    return labelview;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

@end
