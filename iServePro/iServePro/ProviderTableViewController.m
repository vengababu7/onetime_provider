//
//  ProviderTableViewController.m
//  Iservepro
//
//  Created by Rahul Sharma on 31/03/16.
//
//

#import "ProviderTableViewController.h"
#import "LocationTracker.h"
#import "ProviderTableViewCell.h"

@interface ProviderTableViewController ()<UIActionSheetDelegate,CLLocationManagerDelegate>

@property (nonatomic, strong) NSMutableArray *arrayProvide;
@property (nonatomic, strong) NSMutableArray *fixedArray;
@property (nonatomic, strong) NSMutableArray *hourlyArray;
@property (nonatomic, strong) NSMutableArray *mileageArray;

@property (strong, nonatomic) NSMutableArray *pickupDataArray;


@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancel;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *Add;
@property(nonatomic,assign)float currentLatitude;
@property(nonatomic,assign)float currentLongitude;
@property (strong, nonatomic) IBOutlet UITableView *providerTableView;

@end


@implementation ProviderTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
 _pickupDataArray = [[NSMutableArray alloc] init];
    CLLocationManager *lm = [[CLLocationManager alloc] init];
    lm.delegate = self;
    lm.desiredAccuracy = kCLLocationAccuracyBest;
    lm.distanceFilter = kCLDistanceFilterNone;
    [lm startUpdatingLocation];
    [self.Add setTintColor:UIColorFromRGB(0x000000)];

    [self serviceForTypes];
   // [self UpdateAddButtonTitle ];
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    self.navigationItem.leftBarButtonItem = self.cancel;
   
    [self createNavLeftButton];
}
-(void)viewWillAppear:(BOOL)animated
{
//    NSArray *selectedRows =[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedRows"];
//    for (int i=0; i<selectedRows.count; i++) {
//        NSInteger selectedRow = [selectedRows[i]integerValue];
//        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:1]
//                                    animated:NO
//                              scrollPosition:UITableViewScrollPositionNone];
//        [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:1]];
//        
//    }
}
/*-----------------------------------*/
#pragma mark - Navigation Bar methods
/*-----------------------------------*/
- (void)createNavLeftButton
{
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton setImage:[UIImage imageNamed:@"bnt_bck_normal"]
                     forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"bnt_bck_pressed"]
                     forState:UIControlStateSelected];
    
    [navCancelButton addTarget:self action:@selector(backToController) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(10.0f,0.0f,40,40)];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -14;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)backToController
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)serviceForTypes
{
    if (![[NSUserDefaults standardUserDefaults]objectForKey:@"city_ID"]) {
        [Helper showAlertWithTitle:@"Alert" Message:@"Please Select the City"];
        return;
    }
    NSDictionary *queryParams = @{
                                  @"ent_city_id":[[NSUserDefaults standardUserDefaults]objectForKey:@"city_ID"],
                                  @"ent_date_time":[Helper getCurrentDateTime]
                                  };
    TELogInfo(@"param%@",queryParams);
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:@"Getting Providers.."];
    
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    
    [handler composeRequestWithMethod:@"getTypeBYCityId"
                              paramas:queryParams
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             
                             if (!response) {
                                 return;
                             }
                             if (succeeded) {
                                 [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                 [self seperatedCategories:[response mutableCopy]];
                                 [_providerTableView setEditing:YES animated:YES];
                                 _providerTableView.tintColor = UIColorFromRGB(0X008000);
                                 [self updateButtonsToMatchTableState];
                                 
                             }
                             else{
                                 
                                 [Helper showAlertWithTitle:@"Message" Message:response[@"errMsg"]];
                                 
                             }
                         }];
    
}

-(void)seperatedCategories:(NSMutableDictionary *)arrayDict{
    
    _fixedArray =[arrayDict[@"Fixed"] mutableCopy];
    _hourlyArray =[arrayDict[@"Hourly"] mutableCopy];
    _mileageArray =[arrayDict[@"Mileage"] mutableCopy];
    
     [_providerTableView reloadData];
    
}

#pragma mark - UITableViewDelegate
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView * labelview  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 25)];
    UILabel *labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 320, 25)];
    
    [labelview addSubview:labelHeader];
    switch (section) {
        case 0:
            [Helper setToLabel:labelHeader Text:@"Fixed Jobs" WithFont:@"opensans" FSize:14 Color:UIColorFromRGB(0xaaaaaa)];
            labelview.backgroundColor = UIColorFromRGB(0xf8f8f8);
            break;
        case 1:
            [Helper setToLabel:labelHeader Text:@"Hourly Jobs" WithFont:@"opensans" FSize:14 Color:UIColorFromRGB(0xaaaaaa)];
            labelview.backgroundColor = UIColorFromRGB(0xf8f8f8);
            break;
        default:
            [Helper setToLabel:labelHeader Text:@"Mileage Jobs" WithFont:@"opensans" FSize:14 Color:UIColorFromRGB(0xaaaaaa)];
            labelview.backgroundColor = UIColorFromRGB(0xf8f8f8);
            
            break;
    }
     return labelview;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
   // [_pickupDataArray removeObjectAtIndex:indexPath.row];
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[self.tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryNone];
    [self UpdateAddButtonTitle];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return _fixedArray.count;
            break;
        case 1:
            return _hourlyArray.count;
            break;
        default:
            return _mileageArray.count;
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self UpdateAddButtonTitle];
    NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    switch (indexPath.section) {
        case 0://fixed
            [ud setObject:@"3" forKey:@"type"];
            [ud synchronize];
            break;
        case 1://hourly
            [ud setObject:@"2" forKey:@"type"];
            [ud synchronize];
            break;
        default:///mileage
            [ud setObject:@"1" forKey:@"type"];
            [ud synchronize];
            break;
    }
}


- (NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath*)indexPath {
    for ( NSIndexPath* selectedIndexPath in tableView.indexPathsForSelectedRows ) {
        
        if ( selectedIndexPath.section != indexPath.section ){
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
              [Helper showAlertWithTitle:@"Alert" Message:@"You Can Select Categories Under Only One Type"];
            });
            [tableView deselectRowAtIndexPath:selectedIndexPath animated:NO] ;
           [_pickupDataArray removeAllObjects];
        }
    }
    return indexPath ;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger count =indexPath.row;
    NSInteger section  = indexPath.section;
    static NSString *kCellID = @"Provider";
    
    ProviderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    switch (section) {
        case 0:
        {
            NSString *myString = self.fixedArray[count][@"cat_name"];;
            NSString *lower = [myString uppercaseString];
            
            cell.providerCell.text =lower;
            cell.providerType = [NSString stringWithFormat:@"%@",self.fixedArray[count][@"cat_name"]];
        }
            break;
        case 1:
        {
            NSString *myString=self.hourlyArray[count][@"cat_name"];
            NSString *lower = [myString uppercaseString];
            cell.providerCell.text =lower;
            
            cell.providerType = [NSString stringWithFormat:@"%@",self.hourlyArray[count][@"cat_name"]];
        }
            break;
        case 2:
        {
            NSString *myString=self.mileageArray[count][@"cat_name"];
            NSString *lower = [myString uppercaseString];
            
            cell.providerCell.text =lower;
            
            cell.providerType = [NSString stringWithFormat:@"%@",self.mileageArray[count][@"cat_name"]];
        }
            break;
    }
    return cell;
}


#pragma mark - Updating button state

- (void)updateButtonsToMatchTableState
{

    self.navigationItem.rightBarButtonItem = self.Add;
    
}

- (void)UpdateAddButtonTitle
{
    
    NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
    
    BOOL allItemsAreSelected = selectedRows.count == self.arrayProvide.count;
    BOOL noItemsAreSelected = selectedRows.count == 0;
    
    if (allItemsAreSelected || noItemsAreSelected)
    {
        self.Add.title = NSLocalizedString(@"Add", @"");
    }
    else
    {
        NSString *titleFormatString = NSLocalizedString(@"Add (%d)", @"Title for Add button with placeholder for number");
        self.Add.title = [NSString stringWithFormat:titleFormatString, selectedRows.count];
    }
}


- (IBAction)CancelAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)addActiom:(id)sender {
    
    
    if ([[self.tableView indexPathsForSelectedRows] count]) {
    NSString *actionTitle;
    if (([[self.tableView indexPathsForSelectedRows] count] == 1)) {
        actionTitle = NSLocalizedString(@"Are you sure you want to choose these type?", @"");
    }
    else if(([[self.tableView indexPathsForSelectedRows] count] == 0))
    {
        [Helper showAlertWithTitle:@"Message" Message:@"You Haven't Selected Any Category"];
        int noOfProviders = 0;
        while (noOfProviders != _arrayProvide.count) {
              [_providerTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:noOfProviders inSection:0]animated:NO scrollPosition:UITableViewScrollPositionTop];
            noOfProviders++;
        }
      
        actionTitle = NSLocalizedString(@"Are you sure you want to choose these types?", @"");
    }else
    {
        actionTitle = NSLocalizedString(@"Are you sure you want to choose these types?", @"");
    }
    
    NSString *cancelTitle = NSLocalizedString(@"Cancel", @"Cancel title for item removal action");
    NSString *okTitle = NSLocalizedString(@"OK", @"OK title for item removal action");
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:actionTitle
                                                             delegate:self
                                                    cancelButtonTitle:cancelTitle
                                               destructiveButtonTitle:okTitle
                                                    otherButtonTitles:nil];
    
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
    }else{
         [Helper showAlertWithTitle:@"Message" Message:@"You Haven't Selected Any Category"];
    }
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0){
        NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
        NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
        
        switch ([[ud objectForKey:@"type"] integerValue]) {
            case 1://Mielage
                for (int i=0; i<selectedRows.count; i++) {
                    NSIndexPath *indexPath =selectedRows[i];
                    [_pickupDataArray addObject:_mileageArray[indexPath.row]];
                }
                break;
            case 2://hourly
                for (int i=0; i<selectedRows.count; i++) {
                    NSIndexPath *indexPath =selectedRows[i];
                    [_pickupDataArray addObject:_hourlyArray[indexPath.row]];
                }
                break;
            case 3://fixed
                for (int i=0; i<selectedRows.count; i++) {
                    NSIndexPath *indexPath =selectedRows[i];
                    [_pickupDataArray addObject:_fixedArray[indexPath.row]];
                }
                break;
            default:
                break;
        }
        
        NSMutableArray *array = [[NSMutableArray alloc]init];
        for (int i=0; i<selectedRows.count; i++) {
            NSIndexPath *indexPath =selectedRows[i];
            [array addObject:[NSNumber numberWithInt:indexPath.row]];
        }
        
        [ud setObject:array forKey:@"selectedRows"];
        [ud synchronize];
        
        if (buttonIndex == 0)
        {
            NSMutableArray *provider_id = [[NSMutableArray alloc] init];
            NSMutableArray *selectedCat = [[NSMutableArray alloc] init];
            
            NSString *str =@"";
            for (int i=0; i< _pickupDataArray.count; i++) {
                [selectedCat addObject:[self.pickupDataArray objectAtIndex:i][@"cat_name"]];
                [provider_id addObject:[self.pickupDataArray objectAtIndex:i][@"cat_id"]];
                str=[str stringByAppendingString:[self.pickupDataArray objectAtIndex:i][@"cat_id"]];
                str=[str stringByAppendingString:@","];
            }
            
            if ([str hasPrefix:@","]) {
                str = [str substringFromIndex:1];
            }
            
            if ([str hasSuffix:@","]) {
                str = [str substringToIndex:[str length]-1];
            }
            
            //  [[NSUserDefaults standardUserDefaults] setObject:[provider_id mutableCopy] forKey:@"ProviderIDs"];
            [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"ProviderIDs"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            if (self.delegate && [self.delegate respondsToSelector:@selector(providerSelectedTypes:)]) {
                [self.delegate providerSelectedTypes:[selectedCat mutableCopy]];
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

@end
