//
//  AboutViewController.m
//  iServePro
//
//  Created by Rahul Sharma on 02/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "AboutViewController.h"
#import "CustomNavigationBar.h"
#import "iServeSplashController.h"
#import "LocationTracker.h"
#import "User.h"
#import "PaymentTableViewCell.h"

@interface AboutViewController () <CustomNavigationBarDelegate,UserDelegate>
@property (strong, nonatomic) IBOutlet UIButton *jobLogs;
@property (strong, nonatomic) IBOutlet UIButton *paymentLogs;

@end

@implementation AboutViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accountDeactivated) name:@"accountDeactivated" object:nil];
    
}

-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)userDidLogoutSucessfully:(BOOL)sucess {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];
           UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        
        self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
  
}
-(void)userDidFailedToLogout:(NSError *)error {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [[LocationTracker sharedInstance] stopLocationTracking];

        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                    @"Main" bundle:[NSBundle mainBundle]];
        
        iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        
        self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}

-(void)accountDeactivated {
    
    User *user = [User sharedInstance];
    user.delegate = self;
    [user logout];
}

- (IBAction)paymentLogs:(id)sender {
    CGRect frame = _mainScrollView.bounds;
    frame.origin.x = 0;
    [_mainScrollView scrollRectToVisible:frame animated:YES];

}

- (IBAction)jobsLog:(id)sender {
    CGRect frame = _mainScrollView.bounds;
    frame.origin.x = self.view.frame.size.width;
    [_mainScrollView scrollRectToVisible:frame animated:YES];

}
/*---------------------------------*/
#pragma mark - Scrollview Delegate
/*---------------------------------*/
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([scrollView isEqual:_mainScrollView]) {
        
        CGPoint offset = scrollView.contentOffset;
        float widthOfView = CGRectGetWidth(self.view.frame);
        
        // Change Leading position of Seperator/ Selector
        self.leadingContrain.constant = offset.x / 2;
         if (offset.x < widthOfView / 2) {
             
        _paymentLogs.selected = YES;
            _jobLogs.selected = NO;
        }
        else {
            _paymentLogs.selected = NO;
            _jobLogs.selected = YES;
        }
        
        CGFloat minOffsetX = 0;
        CGFloat maxOffsetX = widthOfView * 2;
        
        if (offset.x < minOffsetX) offset.x = minOffsetX;
        if (offset.x > maxOffsetX) offset.x = maxOffsetX;
        
        scrollView.contentOffset = offset;
    }
}

#pragma mark UITableView Delegate -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 40.0f;
    
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView * labelview  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 25)];
    UILabel *labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 6, 320, 22)];
    
    [labelview addSubview:labelHeader];
    
    if (section == 0) {
        [Helper setToLabel:labelHeader Text:@"PENDING PAYMENTS" WithFont:@"opensans" FSize:11 Color:UIColorFromRGB(0xaaaaaa)];
        labelview.backgroundColor = UIColorFromRGB(0xf8f8f8);
    }
        else if (section == 1){
    
            [Helper setToLabel:labelHeader Text:@"LAST PAYMENTS" WithFont:@"opensans" FSize:11 Color:UIColorFromRGB(0xaaaaaa)];
            labelview.backgroundColor = UIColorFromRGB(0xf8f8f8);
        }
    
    return labelview;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    

}


#pragma mark UITableview DataSource -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 5;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"cellForRowAtIndexPath");
    static NSString *CellIdentifier = @"payment";
    PaymentTableViewCell *cell;
    if (!cell) {
        cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
  
    return cell;
}

@end
