//
//  InvoicePopup.m
//  iServePro
//
//  Created by Rahul Sharma on 12/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "InvoicePopup.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "InvoiceHistTableViewCell.h"

@implementation InvoicePopup

static  InvoicePopup *share;


+ (id)sharedInstance
{
    if (!share ) {
        share=[[self alloc]init];
    }
    return share;
}
- (instancetype)init{
    self = [[[NSBundle mainBundle] loadNibNamed:@"invoice"
                                          owner:self
                                        options:nil] firstObject];
    [self.serviceTable registerNib:[UINib nibWithNibName:@"HistoryCell" bundle:nil] forCellReuseIdentifier:@"services"];
    return self;
}
- (void)showPopUpWithDictionary:(NSDictionary *)shipments
                       onWindow:(UIWindow *)window {
    self.frame = window.frame;
    [window addSubview:self];
    _serviceDetails = [[NSMutableArray alloc]init];
    _serviceDetails = shipments[@"services"];
    _serviceTot = 0;
    _bidLabel.text =[NSString stringWithFormat:@"BID: %@", shipments[@"bid"]];
    
    _custAddress.text=shipments[@"addrLine1"];
    _custName.text=shipments[@"fname"];
    _phoneNo.text=shipments[@"phone"];
     _bidLabel.text =[NSString stringWithFormat:@"BID: %@", shipments[@"bid"]];
    NSString *strImageUrl =flStrForObj(shipments[@"sign_url"]);
    [_signatureImg sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                     placeholderImage:[UIImage imageNamed:@""]
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                
                            }];
    
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    
    NSNumber *timeFee =[NSNumber numberWithInteger:[flStrForObj(shipments[@"appt_duration"]) integerValue]*[flStrForObj(shipments[@"price_per_min"]) integerValue]];
    NSString *totTime = [formatter stringFromNumber:timeFee];
    _timeFee.text=totTime;
    
    
    
    NSNumber *mateFee =[NSNumber numberWithInteger:[flStrForObj(shipments[@"mat_fees"]) integerValue]];
    NSString *totalMat = [formatter stringFromNumber:mateFee];
    _matFee.text=totalMat;
    
    NSNumber *miscFee =[NSNumber numberWithInteger:[flStrForObj(shipments[@"misc_fees"]) integerValue]];
    NSString *totalMisc = [formatter stringFromNumber:miscFee];
    _miscFee.text=totalMisc;
    
    NSNumber *proDisc =[NSNumber numberWithInteger:[flStrForObj(shipments[@"pro_disc"]) integerValue]];
    NSString *proDiscount = [formatter stringFromNumber:proDisc];
    _proDisc.text=proDiscount;
    
    NSNumber *disc =[NSNumber numberWithInteger:[flStrForObj(shipments[@"coupon_discount"]) integerValue]];
    NSString *discCoupon = [formatter stringFromNumber:disc];
    _disc.text=discCoupon;
    
    
    if ([shipments[@"payment_type"]integerValue]==1) {
        _payType.text=@"CASH";
    }else{
        _payType.text=@"CARD";
    }
    _notes.text=flStrForObj(shipments[@"pro_notes"]);
    
    
    NSNumber *visAmt =[NSNumber numberWithInteger:[flStrForObj(shipments[@"visit_amount"]) integerValue]];
    NSString *visAmount = [formatter stringFromNumber:visAmt];
    _visitFee.text=visAmount;
    
    for (int i = 0 ; i<_serviceDetails.count; i++) {
        _serviceTot = _serviceTot + [_serviceDetails [i][@"sprice"]integerValue];
    }
    
    NSNumber *subTot =[NSNumber numberWithFloat:([timeFee floatValue] + [visAmt floatValue]+[mateFee floatValue]+[miscFee floatValue]+_serviceTot)];
    NSString *subTotal = [formatter stringFromNumber:subTot];
    _subTot.text=subTotal;
    
    NSNumber *tot =[NSNumber numberWithFloat:([subTot floatValue] - [proDisc floatValue]-[disc floatValue])];
    NSString *total = [formatter stringFromNumber:tot];
    _total.text=total;
    
    [UIView animateWithDuration:0.3
                          delay:0.1
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                         [self layoutIfNeeded];
                     }
                     completion:^(BOOL finished){
                         [self layoutIfNeeded];
                     }];
    [_serviceTable reloadData];
    
    self.heightofTableView.constant = self.serviceTable.contentSize.height;
    [self layoutIfNeeded];
    
    
}

- (IBAction)closePopup:(id)sender {
    self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    
    [UIView animateWithDuration:0.3
                          delay:0.1
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.01, 0.01);
                     }
                     completion:^(BOOL finished) {
                         share = nil;
                         [self removeFromSuperview];
                     }];
    
}
#pragma mark UITableview DataSource -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return  [_serviceDetails count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"services";
    InvoiceHistTableViewCell *cell;
    if (!cell) {
        cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[NSLocale currentLocale]];
    
    cell.serviceName.text = _serviceDetails[indexPath.row][@"sname"];
    
    NSNumber *ServicePrice =[NSNumber numberWithInteger:[ _serviceDetails[indexPath.row][@"sprice"] integerValue]];
    NSString *price = [formatter stringFromNumber:ServicePrice];
    cell.serviceVal.text = price;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (_serviceDetails.count !=0) {
        UIView * labelview  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 25)];
        UILabel *labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(12, 3, 320, 22)];
        
        [labelview addSubview:labelHeader];
        
        if (section == 0) {
            [Helper setToLabel:labelHeader Text:@"SELECTED SERVICES" WithFont:@"opensans" FSize:11 Color:UIColorFromRGB(0xaaaaaa)];
            labelview.backgroundColor = UIColorFromRGB(0xf8f8f8);
        }
        return labelview;
    }else{
        return nil;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (_serviceDetails.count !=0) {
        return 30;
    }else{
        return 0;
    }
}
@end
