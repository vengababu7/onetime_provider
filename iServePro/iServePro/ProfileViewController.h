//
//  ProfileViewController.h
//  iServePro
//
//  Created by Rahul Sharma on 28/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UploadFiles.h"
#import "RoundedImageView.h"
#import "AXRatingView.h"

@interface ProfileViewController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UITextViewDelegate>
{
    BOOL textFieldEditedFlag;
}

@property (strong, nonatomic) IBOutlet UILabel *proName;
@property (strong, nonatomic) IBOutlet UILabel *appVersion;

@property (strong, nonatomic) IBOutlet UILabel *catergoies;
@property (strong, nonatomic) IBOutlet UIImageView *profileImgView;

@property (strong, nonatomic) IBOutlet UILabel *noOfReviews;
@property (strong, nonatomic)NSMutableArray *profileArray;

@property (strong, nonatomic) IBOutlet AXRatingView *ratingView;
@property (strong, nonatomic) IBOutlet UIButton *logoutButton;

@property (strong, nonatomic) UIButton *addButton;

- (IBAction)addPhotosButton:(id)sender;

- (IBAction)logoutAction:(id)sender;
-(void)getProfileData;
@end
