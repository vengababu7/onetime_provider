//
//  TimerViewController.h
//  iServePro
//
//  Created by Rahul Sharma on 06/07/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimerViewController : UIViewController
@property(strong, nonatomic) NSMutableDictionary *dictBookingDetails;
@property (strong, nonatomic) IBOutlet UIView *sliderView;
@property (strong, nonatomic) IBOutlet UILabel *timerLabel;

- (IBAction)pauseTimer:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *jobDetails;
- (IBAction)messageAction:(id)sender;
- (IBAction)callAction:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *custName;

@property (strong, nonatomic) IBOutlet UIButton *pauseTimerLabel;

@property (strong, nonatomic) IBOutlet UILabel *noJobPhotos;
@property (strong, nonatomic) NSString *timerTag;
@end
