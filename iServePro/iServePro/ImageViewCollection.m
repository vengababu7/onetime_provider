//
//  ImageViewCollection.m
//  YaaroDriver
//
//  Created by Rahul Sharma on 05/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

#import "ImageViewCollection.h"
#import "CellCollectionXib.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AmazonTransfer.h"


@implementation ImageViewCollection

static ImageViewCollection *share;


+(id)sharedInstance
{
    if (!share ) {
        share=[[self alloc]init];
    }
    return share;
}
- (instancetype)init{
    self = [[[NSBundle mainBundle] loadNibNamed:@"ImagesPopup"
                                          owner:self
                                        options:nil] firstObject];
    
    [self.imageCollection registerNib:[UINib nibWithNibName:@"collectionCell" bundle:nil] forCellWithReuseIdentifier:@"images"];
    
    return self;
}

- (void)showPopUpWithDictionary:(UIWindow *)window jobImages:(NSInteger)images index:(NSIndexPath*)indexVal tag:(NSInteger)tag{
    _tagValue=tag;
    _jobImages =images;
    self.frame = window.frame;
    [window addSubview:self];
    self.contentView.alpha = 0.3;
    self.contentView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.95, 0.95);
    [UIView animateWithDuration:0.1
                     animations:^{
                         self.contentView.alpha = 1;
                         self.imageCollection.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1,1);
                         
                     }
                     completion:^(BOOL finished) {
                         NSString *strImageUrl;
                         if (_tagValue==1) {
                             strImageUrl   =[NSString stringWithFormat:@"https://s3.amazonaws.com/iserve/jobImages/%@_%ld.png",[[NSUserDefaults standardUserDefaults] objectForKey:@"proid"],(long)indexVal.row] ;
                         }else{
                             strImageUrl = [NSString stringWithFormat:@"https://s3.amazonaws.com/%@/JobImages/%@_%ld.png",Bucket,[[NSUserDefaults standardUserDefaults] objectForKey:@"BID"],(long)indexVal.row];
                         }
                         
                         [_selectedImageView sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                                               placeholderImage:[UIImage imageNamed:@"user_image_default"]
                                                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                                          
                                                      }];
                         [self.imageCollection reloadData];
                         [self layoutIfNeeded];
                         
                     }];
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _jobImages;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CellCollectionXib *cell;
    cell=[self.imageCollection dequeueReusableCellWithReuseIdentifier:@"images" forIndexPath:indexPath];
    if (!cell) {
        NSArray *arrayOfXib=  [[NSBundle mainBundle] loadNibNamed:@"collectionCell"
                                                            owner:self
                                                          options:nil];
        cell = [arrayOfXib objectAtIndex:0];
    }
    NSString *strImageUrl;
    if (_tagValue==1) {
          strImageUrl=[NSString stringWithFormat:@"https://s3.amazonaws.com/iserve/jobImages/%@_%ld.png",[[NSUserDefaults standardUserDefaults] objectForKey:@"proid"],(long)indexPath.row] ;
    }else{
        strImageUrl = [NSString stringWithFormat:@"https://s3.amazonaws.com/%@/JobImages/%@_%ld.png",Bucket,[[NSUserDefaults standardUserDefaults] objectForKey:@"BID"],(long)indexPath.row];
    }
    [cell.jobImages sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                          placeholderImage:[UIImage imageNamed:@"user_image_default"]
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                     
                                 }];

 

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strImageUrl;
    if (_tagValue==1) {
        strImageUrl=[NSString stringWithFormat:@"https://s3.amazonaws.com/iserve/jobImages/%@_%ld.png",[[NSUserDefaults standardUserDefaults] objectForKey:@"proid"],(long)indexPath.row] ;
    }else{
        strImageUrl = [NSString stringWithFormat:@"https://s3.amazonaws.com/%@/JobImages/%@_%ld.png",Bucket,[[NSUserDefaults standardUserDefaults] objectForKey:@"BID"],(long)indexPath.row];
    }
    [_selectedImageView sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                          placeholderImage:[UIImage imageNamed:@"user_image_default"]
                                 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                     
                                 }];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(80,80);
}

- (IBAction)closePopup:(id)sender {
    [self removeFromSuperview];
}


@end
