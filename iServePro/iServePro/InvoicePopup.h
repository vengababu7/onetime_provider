//
//  InvoicePopup.h
//  iServePro
//
//  Created by Rahul Sharma on 12/09/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InvoicePopup : UIView
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *custName;
@property (strong, nonatomic) IBOutlet UILabel *phoneNo;
@property (strong, nonatomic) IBOutlet UILabel *custAddress;
@property (strong, nonatomic) IBOutlet UILabel *visitFee;
@property (strong, nonatomic) IBOutlet UILabel *timeFee;
@property (strong, nonatomic) IBOutlet UILabel *matFee;
@property (strong, nonatomic) IBOutlet UILabel *miscFee;
@property (strong, nonatomic) IBOutlet UILabel *subTot;
@property (strong, nonatomic) IBOutlet UILabel *proDisc;
@property (strong, nonatomic) IBOutlet UILabel *disc;
@property (strong, nonatomic) IBOutlet UILabel *total;
@property (strong, nonatomic) IBOutlet UILabel *payType;
@property (strong, nonatomic) IBOutlet UIImageView *signatureImg;
- (IBAction)closePopup:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *notes;

+ (id)sharedInstance;
- (void)showPopUpWithDictionary:(NSDictionary *)shipments
                       onWindow:(UIWindow *)window;

@property (strong, nonatomic) IBOutlet UILabel *bidLabel;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightofTableView;

@property (strong, nonatomic) IBOutlet UITableView *serviceTable;
@property (strong, nonatomic) NSMutableArray *serviceDetails;
@property (assign, nonatomic) NSInteger serviceTot;
@end
