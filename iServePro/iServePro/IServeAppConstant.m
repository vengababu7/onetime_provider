//
//  IServeAppConstant.m
//  iServePro
//
//  Created by Rahul Sharma on 25/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IServeAppConstant.h"

#pragma mark - Constants

NSString *const kISPSocketChannel = @"UpdateProvider";
NSString *const kPMDPubNubPublisherKey   = @"pub-c-9546795a-2ee3-4967-98f8-7bffe970e118";
NSString *const kPMDPubNubSubcriptionKey = @"sub-c-776f16fc-ac6a-11e3-aaa5-02ee2ddab7fe";
NSString *const kPMDGoogleMapsAPIKey     = @"AIzaSyAWjgVu6Wd609tCOYkbZlHZ2yIRl7kBpX0";
NSString *const kPMDCrashLyticsAPIKey     = @"8c41e9486e74492897473de501e087dbc6d9f391";
NSString *const kPMDTestDeviceidKey = @"C2A33350-D9CF-4A7E-8751-A36016838381";
NSString *const kPMDDeviceIdKey = @"deviceid";

#pragma mark - mark URLs
NSString *BASE_URL_RESTKIT                   = BASE_IP ;

NSString *BASE_URL                           =@"http://54.164.60.116/test/services.php/";
//@"http://www.iserve.ind.in/services_v7.php/";




NSString *const   baseUrlForXXHDPIImage      = BASE_IP @"pics/xxhdpi/";
NSString *const   baseUrlForOriginalImage    = BASE_IP @"pics/";
NSString *const   baseUrlForThumbnailImage   = @"http://www.iServePro.com/pics/xhdpi/";
NSString *const   baseUrlForUploadImage      = BASE_IP @"testProcess.php/";

#pragma mark - ServiceMethods
// eg : prifix kSM
NSString *const kSMLiveBooking                = @"liveBooking";
NSString *const kSMGetAppointmentDetial       = @"getAppointmentDetails";
NSString *const kSMUpdateSlaveReview          = @"updateSlaveReview";
NSString *const kSMGetMasters                 = @"getMasters";

//SignUp

NSString *KDASignUpFirstName                  = @"ent_first_name";
NSString *KDASignUpLastName                   = @"ent_last_name";
NSString *KDASignUpMobile                     = @"ent_mobile";
NSString *KDASignUpEmail                      = @"ent_email";
NSString *KDASignUpPassword                   = @"ent_password";
NSString *KDASignUpAddLine1                   = @"ent_address_line1";
NSString *KDASignUpAddLine2                   = @"ent_address_line2";
NSString *KDASignUpAccessToken                = @"ent_token";
NSString *KDASignUpDateTime                   = @"ent_date_time";
NSString *KDASignUpCountry                    = @"ent_country";
NSString *KDASignUpCity                       = @"ent_city";
NSString *KDASignUpDeviceType                 = @"ent_device_type";
NSString *KDASignUpDeviceId                   = @"ent_dev_id";
NSString *KDASignUpPushToken                  = @"ent_push_token";
NSString *KDASignUpZipCode                    = @"ent_zipcode";
NSString *KDASignUpCreditCardNo               = @"ent_cc_num";
NSString *KDASignUpCreditCardCVV              = @"ent_cc_cvv";
NSString *KDASignUpCreditCardExpiry           = @"ent_cc_exp";
NSString *KDASignUpTandC                      = @"ent_terms_cond";
NSString *KDASignUpPricing                    = @"ent_pricing_cond";
NSString *KDASignUpLattitude                  = @"ent_latitude";
NSString *KDASignUpLongitude                  = @"ent_longitude";
NSString *KDASignUpDoctorType                 = @"ent_service_type";

// Login

NSString *KDALoginEmail                       = @"ent_email";
NSString *KDALoginPassword                    = @"ent_password";
NSString *KDALoginDeviceType                  = @"ent_device_type";
NSString *KDALoginDevideId                    = @"ent_dev_id";
NSString *KDALoginPushToken                   = @"ent_push_token";
NSString *KDALoginUpDateTime                  = @"ent_date_time";

//Upload
NSString *KDAUploadDeviceId                     = @"ent_dev_id";
NSString *KDAUploadSessionToken                 = @"ent_sess_token";
NSString *KDAUploadImageName                    = @"ent_snap_name";
NSString *KDAUploadImageChunck                  = @"ent_snap_chunk";
NSString *KDAUploadfrom                         = @"ent_upld_from";
NSString *KDAUploadtype                         = @"ent_snap_type";
NSString *KDAUploadDateTime                     = @"ent_date_time";
NSString *KDAUploadOffset                       = @"ent_offset";

// Logout the user

NSString *KDALogoutSessionToken                = @"user_session_token";
NSString *KDALogoutUserId                      = @"logout_user_id";


//Parsms for checking user logged out or not

NSString *KDAcheckUserId                        = @"user_id";
NSString *KDAcheckUserSessionToken              = @"ent_sess_token";
NSString *KDAgetPushToken                       = @"ent_push_token";

//Params to store the Country & City.

NSString *KDACountry                            = @"country";
NSString *KDACity                               = @"city";
NSString *KDALatitude                           = @"latitudeQR";
NSString *KDALongitude                          = @"longitudeQR";

//params for Firstname
NSString *KDAFirstName                          = @"ent_first_name";
NSString *KDALastName                           = @"ent_last_name";
NSString *KDAEmail                              = @"ent_email";
NSString *KDAPhoneNo                            = @"ent_mobile";
NSString *KDAPassword                           = @"ent_password";



#pragma mark - NSUserDeafults Keys

NSString *const kNSUAppoinmentDoctorDetialKey         = @"doctorDetial";
NSString *const kNSUDoctorEmailAddressKey            = @"dEmail";
NSString *const kNSUDoctorProfilePicKey               = @"dProfilePic";
NSString *const kNSUDoctorNameKey                     = @"dDoctorName";
NSString *const kNSUDoctorPhonekey                    = @"doctorPhone";
NSString *const kNSUDoctorTypekey                     = @"doctorType";
NSString *const kNSUMongoDataBaseAPIKey               = @"mongoDBapi";
NSString *const kNSUPatientPubNubChannelkey            = @"patChn";
NSString *const kNSUDoctorSubscribeChanelKey            = @"dSubChannel";
NSString *const kNSUDoctorServerChanelKey               =@"serverChn";



#pragma mark - PushNotification Payload Keys

NSString *const kPNPayloadDoctorNameKey            = @"sname";
NSString *const kPNPayloadAppoinmentTimeKey        = @"dt";
NSString *const kPNPayloadDistanceKey              = @"dis";
NSString *const kPNPayloadEstimatedTimeKey         = @"eta";
NSString *const kPNPayloadDoctorEmailKey           = @"e";
NSString *const kPNPayloadDoctorContactNumberKey   = @"ph";
NSString *const kPNPayloadProfilePictureUrlKey     = @"pic";
NSString *const kPNPayloadAppoinmentDateStringKey  = @"dt";
NSString *const kPNPayloadAppoinmentLatitudeKey    = @"ltg";


#pragma mark - Controller Keys

#pragma mark - Network Error

NSString *const kNetworkErrormessage          = @"No network connection";

