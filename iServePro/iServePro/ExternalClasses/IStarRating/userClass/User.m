//
//  User.m
//  privMD
//
//  Created by Surender Rathore on 19/04/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "User.h"
//#import "Database.h"
#import "PMDReachabilityWrapper.h"
#import "NetworkHandler.h"
#import "ChatSocketIOClient.h"



@implementation User
@synthesize delegate;
@synthesize blurredImage;
@synthesize profileImage;
@synthesize name;
@synthesize email;
@synthesize phone;


static User  *user;

+ (id)sharedInstance {
    
	if (!user) {
		user  = [[self alloc] init];
	}
	
	return user;
}


- (void)logout
{
       
    NSDictionary *queryParams;
    queryParams = [NSDictionary dictionaryWithObjectsAndKeys:
                   [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],KDAcheckUserSessionToken,
                   [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],kSMPCommonDevideId,
                   [NSNumber numberWithInt:1],kSMPPassengerUserType,
                   [Helper getCurrentDateTime],kSMPCommonUpDateTime, nil];
    
    TELogInfo(@"param%@",queryParams);
    
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    [handler composeRequestWithMethod:MethodLogout paramas:queryParams onComplition:^(BOOL succeeded, NSDictionary *response) {
        ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
        [progressIndicator hideProgressIndicator];

        if (succeeded) {
            
            [self logoutUserResponse:response];
        }
        
    }];
    
}

-(void)logoutUserResponse:(NSDictionary *)response {
    
    if ([response[@"errFlag"] integerValue] == 0) {
        
        if (delegate && [delegate respondsToSelector:@selector(userDidLogoutSucessfully:)]) {
            [delegate userDidLogoutSucessfully:YES];
            
        }

    }else if ([response[@"errFlag"] integerValue] == 1 ) {
        
        if (delegate && [delegate respondsToSelector:@selector(userDidLogoutSucessfully:)]) {
            [delegate userDidLogoutSucessfully:YES];
        }

    }
    
    
}



- (void) userLogoutResponse:(NSArray *)response
{
    
   // TELogInfo(@"_response%@",response);
    Errorhandler * handler = [response objectAtIndex:0];
    
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    
    if ([[handler errFlag] intValue] ==0) {
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"LoggedIn"];
        if (delegate && [delegate respondsToSelector:@selector(userDidLogoutSucessfully:)]) {
            [delegate userDidLogoutSucessfully:YES];
           [[NSUserDefaults standardUserDefaults ] removeObjectForKey:@"driverName"];
        }
        
        
    }
    else if ([[handler errFlag] intValue] ==1)
    {
        if (delegate && [delegate respondsToSelector:@selector(userDidLogoutSucessfully:)]) {
            [delegate userDidLogoutSucessfully:YES];
            [[NSUserDefaults standardUserDefaults ] removeObjectForKey:@"driverName"];//
        }
        
    }

    else
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:handler.errMsg];
    }
    

    
   }


-(void)deleteUserSavedData{
    
    //delete all saved cards
    //[Database deleteAllCard];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
