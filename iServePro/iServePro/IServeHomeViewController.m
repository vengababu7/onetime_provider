//
//  IServeHomeViewController.m
//  iServePro
//
//  Created by Rahul Sharma on 25/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "IServeHomeViewController.h"
#import "UploadFiles.h"
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
#import "CustomMarkerView.h"
#import "Helper.h"
#import "UIImageView+WebCache.h"
#import "LocationTracker.h"
#import  "iServeSplashController.h"
#import "HomeCell.h"
#import "AppointmentDetailController.h"
#import "ChatSocketIOClient.h"
#import "BookingViewController.h"
#import "InvoiceViewController.h"
#import "TimerViewController.h"
#import "LocationServicesViewController.h"
#import <EventKit/EventKit.h>
#import "ChatSocketIOClient.h"
#import "ChatSIOClient.h"



#define mapZoomLevel 17
#define BottomShadowImageTag 18

@interface IServeHomeViewController ()<CLLocationManagerDelegate,GMSMapViewDelegate,HomeCellDelegate,UITableViewDelegate,UITableViewDataSource>

{
    NSMutableArray *waypoints_;
    NSMutableArray *waypointStrings_;
    GMSMapView *mapView_;
    NSArray *bookingArray;
    NSArray *newAppointments;
    UIView *customMapView;
    Boolean isMapViewOpened;
    NSArray *seguesDict;
     CGRect screenSize;
    
     NSArray *laterBookingArray;
    ChatSocketIOClient *socket;
     ChatSIOClient *soketIOClient;
    
}

@property (strong, nonatomic) IBOutlet UILabel *noAppoointmentLabel;
@property(nonatomic,strong)CLLocationManager *locationManager;
@property(nonatomic,assign)CLLocationCoordinate2D previouCoord;
@property(nonatomic,assign) float currentLatitude;
@property(nonatomic,assign) float currentLongitude;
@property LocationTracker * locationTracker;
@property (nonatomic) NSTimer* locationUpdateTimer;
@property (nonatomic, strong) UIRefreshControl *refreshControl;


@end

@implementation IServeHomeViewController

- (void)viewDidLoad {
   socket  =[ChatSocketIOClient sharedInstance];
    soketIOClient =[ChatSIOClient sharedInstance];
    [super viewDidLoad];
    [self getCustomerAppointment];
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    _counter = 0;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    CLLocationCoordinate2D coordinate = [self getLocation];
    self.currentLatitude = coordinate.latitude;
    self.currentLongitude = coordinate.longitude;
    [[NSUserDefaults standardUserDefaults] synchronize];
    waypoints_ = [[NSMutableArray alloc]init];
    waypointStrings_ = [[NSMutableArray alloc]initWithCapacity:2];
    seguesDict =@[@"appointmentDetail",@"ToMapController",@"ToTimerVC",@"ToInvoice"];
}


- (void)viewWillAppear:(BOOL)animated
{
    [socket socketIOSetup];
    [self getCustomerAppointment];
    dispatch_async(dispatch_get_main_queue(), ^{
        mapView_.myLocationEnabled = YES;
    });
    //NEW BOOKING
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCustomerAppointment) name:@"DismissNewBookingVC" object:nil];
    
    
    NSString *bookingDate = [Helper getCurrentDateTime:[NSDate date] dateFormat:@"MM/dd/YY"];
    NSLog(@"viewWillAppear%@",bookingDate);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCustomerAppointment) name:@"AppointmentCancel" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handelAcceptRejectFromAdmin:) name:@"HandleAcceptAndRejectFromAdmin" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTable) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTable) name:@"AppointmentAcceptAndRejectFromAdmin" object:nil];
}


-(void)viewDidAppear:(BOOL)animated
{
    if (!soketIOClient.isSocketConnected) {
        [socket socketIOSetup];
    }

    [self getCustomerAppointment];
    if (IS_SIMULATOR)
    {
        _currentLatitude = 13.028866;
        _currentLongitude = 77.589760;
        CLLocationCoordinate2D locationCord = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
        mapView_.camera = [GMSCameraPosition cameraWithTarget:locationCord
                                                         zoom:14];
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
        marker.title = NSLocalizedString(@"It's Me", @"It's Me");
        marker.snippet = NSLocalizedString(@"Current Location", @"Current Location");
        marker.map = mapView_;
    }
    else
    {
        NSLog(@"latitude %f",_currentLatitude);
        NSLog(@"logitude %f",_currentLongitude);
    }
    _previouCoord = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [socket heartBeat:@"1"];
    });
    dispatch_async(dispatch_get_main_queue(), ^{
        mapView_.myLocationEnabled = YES;
    });
}


- (void)viewWillDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AppointmentCancel" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"HandleAcceptAndRejectFromAdmin" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DismissNewBookingVC" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AppointmentAcceptAndRejectFromAdmin" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [mapView_ removeObserver:self forKeyPath:@"myLocation" context:NULL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma Refresh

- (void)refreshTable{
    if ([Helper isConnected]) {
       [self getCustomerAppointment];
        
    }else{
        [Helper showAlertViewForInternetCheck];
    }
    
}

-(void)checkLocationServices  {
    
    LocationServicesViewController *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
    UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
    [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
    
}
- (void) locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    
    
    if ([CLLocationManager locationServicesEnabled]) {
        switch ([CLLocationManager authorizationStatus]) {
            case kCLAuthorizationStatusAuthorizedAlways:
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationServices" object:nil];
                break;
            case kCLAuthorizationStatusDenied:
                [self checkLocationServices];
                break;
            case kCLAuthorizationStatusAuthorizedWhenInUse:
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationServices" object:nil];
                break;
            case kCLAuthorizationStatusNotDetermined:
                NSLog(@"Not Determined");
                break;
            case kCLAuthorizationStatusRestricted:
                NSLog(@"Restricted");
                break;
        }
        
    }else{
        LocationServicesViewController *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
        UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
        [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
    }
}


#pragma handleAcceptRejectFromAdmin

- (void)handelAcceptRejectFromAdmin:(NSNotification *)notification{
    
    NSDictionary *dict = notification.userInfo;
    
    
    
    int type = [dict[@"aps"][@"nt"] intValue];
    if (type == 11) { // 11 == accept , 12 == reject
        
        [Helper showAlertWithTitle:@"Accepted!" Message:dict[@"alert"]];
        UIView *transparentView = [self.view viewWithTag:500];
        if (transparentView) {
            
            [transparentView removeFromSuperview];
        }
        
    }else{
        [Helper showAlertWithTitle:@"Rejected!" Message:dict[@"alert"]];
        [self addTransparentViewWhenProfileIsUnderVerification:dict[@"alert"]];
    }
}
#pragma mark Get Doctor current status

-(NSString *)getMonths
{
    NSDate *date = [NSDate date];
    NSCalendar *calendarLoc = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendarLoc components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    NSInteger Day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    //
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
    NSString * monthString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:month]];
    NSString * dayString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:Day]];
    
    NSString *retMonth = [NSString stringWithFormat:@"%ld-%@-%@",(long)year,monthString,dayString];
    return retMonth;
}
#pragma mark Webservice Request -
-(void)getCustomerAppointment
{
    NSLog(@">>>>>>>>>>>>>>>>>>>>>>>>" );
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Loading.."];
 //   NSString *month = [self getMonths];
    
    NSDictionary *parameters = @{
                                 kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                 kSMPCommonDevideId:[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                 @"ent_date_time":[Helper getCurrentDateTime]
                                 };
    
    NSLog(@"getCustomerAppointment>>>>>>>>>> %@",parameters);
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    
    [handler composeRequestWithMethod:@"getMasterAppointmentsHome"
                              paramas:parameters
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             
                             if (succeeded)
                             {
                                 [self getCustomerAppointmentResponse:response];
                             }
                             else{
                                 [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                 NSLog(@"Error");
                             }
                         }];
}


-(void)getCustomerAppointmentResponse:(NSDictionary *)response
{
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"Badge"];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    [self.refreshControl endRefreshing];
    
    NSLog(@"response Appointment:%@",response);
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    else if ([[response objectForKey:@"errFlag"] intValue] == 1 && ([[response objectForKey:@"errNum"] intValue] == 6|| [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 83 || [response[@"errNum"] intValue] == 76))
    {
        [self performSelector:@selector(userSessionTokenExpire) withObject:nil afterDelay:1];
        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Session Expired" message:@"Your session with IServe is expired. Please Login again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alert show];
        
        
    }
    else if ([[response objectForKey:@"errFlag"] intValue] == 1 && ([[response objectForKey:@"errNum"] intValue] == 6|| [response[@"errNum"] intValue] == 11 || [response[@"errNum"] intValue] == 76 ))
    {
        [self addTransparentViewWhenProfileIsUnderVerification:response[@"errMsg"]];
    }
    else
    {
        NSDictionary *dictResponse  = response;
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            bookingArray = dictResponse[@"nowbooking"];
            laterBookingArray = dictResponse[@"laterBookings"];
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            NSLog(@"tableView1:%@",self.tblView);
        }
        else{
            bookingArray=@[];
            laterBookingArray=@[];
        }
    }
    self.tblView.dataSource = nil;
    self.tblView.delegate = nil;
    
    self.tblView.dataSource = self;
    self.tblView.delegate = self;
    
    [self.tblView reloadData];
    self.heightOfContent.constant = self.tblView.contentSize.height;
    [self.view layoutIfNeeded];
}


-(void)remaindersForCalendar{
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) { return; }
        EKEvent *event = [EKEvent eventWithEventStore:store];
        event.title = @"IServe Provider";
        event.startDate = [NSDate date]; //today
        event.endDate = [event.startDate dateByAddingTimeInterval:60*60];  //set 1 hour meeting
        event.calendar = [store defaultCalendarForNewEvents];
        NSError *err = nil;
        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
     // self.savedEventId = event.eventIdentifier;  //save the event id if you want to access this later
    }];
}
#pragma mark Other Methods -
-(void)userSessionTokenExpire{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
    
}



- (void)addTransparentViewWhenProfileIsUnderVerification:(NSString *)message{
    UIView *transparentView = [self.view viewWithTag:500];
    if (!transparentView) {
        UIView *transparentView = [[UIView alloc]initWithFrame:CGRectMake(10, [UIScreen mainScreen].bounds.size.height - 60, 300, 55)];
        [transparentView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.6]];
        transparentView.tag = 500;
        // border radius
        [transparentView.layer setCornerRadius:15.0f];
        // border
        [transparentView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [transparentView.layer setBorderWidth:1.5f];
        [self.view addSubview:transparentView];
        UILabel *labelMessage = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 300, 44)];
        labelMessage.textAlignment = NSTextAlignmentCenter;
        labelMessage.numberOfLines = 2;
        labelMessage.tag = 100;
        [Helper setToLabel:labelMessage Text:message WithFont:HELVETICANEUE_LIGHT FSize:13 Color:WHITE_COLOR];
        [transparentView addSubview:labelMessage];
    }else{
        
        UILabel  *label = (UILabel *)[transparentView viewWithTag:100];
        label.text = message;
    }
    
}


-(CLLocationCoordinate2D)getLocation
{
    
    if ([CLLocationManager locationServicesEnabled]) {
        
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                
                [_locationManager requestAlwaysAuthorization];
            }
            if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [_locationManager requestWhenInUseAuthorization];
            }
        }
    }
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    _locationManager.distanceFilter = 50;
    _locationManager.headingFilter = 1;
    [_locationManager startUpdatingLocation];
    [_locationManager startUpdatingHeading];
    CLLocation *location = [_locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    return coordinate;
}

#pragma get font Details
-(void)getFontFamily {
    
    NSArray *familyNames = [[NSArray alloc] initWithArray:[UIFont familyNames]];
    NSArray *fontNames;
    NSInteger indFamily, indFont;
    for (indFamily = 0; indFamily < [familyNames count]; ++indFamily)
    {
        NSLog(@"********Family name: %@", [familyNames objectAtIndex:indFamily]);
        fontNames = [[NSArray alloc] initWithArray:[UIFont fontNamesForFamilyName:[familyNames objectAtIndex:indFamily]]];
        for (indFont=0; indFont<[fontNames count]; ++indFont)
        {
            NSLog(@"----------Font name: %@", [fontNames objectAtIndex:indFont]);
        }
    }
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    NSInteger segueIndex =[seguesDict indexOfObject:[segue identifier]];
    switch (segueIndex) {
        case 0:
        {
            AppointmentDetailController *details = [segue destinationViewController];
            details.dictAppointmentDetails = sender;
        }
            break;
        case 1:
        {
            BookingViewController *details =[segue destinationViewController];
            details.dictBookingDetails =sender;
        }
            break;
        case 2:
        {
            TimerViewController *details =[segue destinationViewController];
            details.dictBookingDetails = sender;
        }
            break;
        case 3:
        {
            InvoiceViewController *details =[segue destinationViewController];
            details.dictBookingDetails = sender;
        }
            break;
    }
}

#pragma mark UITableView Delegate -

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    static NSString *CellIdentifier = @"HomeViewCell";
    //    HomeCell *cell=(HomeCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //    if (!cell) {
    //        cell =[[HomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    //    }
    //    cell.bookingaddress.text=bookingArray[indexPath.row][@"addrLine1"];
    //    float height = [self measureHeightLabel:cell.bookingaddress];
    return 123;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    
    UIView * labelview  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 25)];
    UILabel *labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 6, 320, 22)];
    
    [labelview addSubview:labelHeader];
    
    if (section == 0) {
        [Helper setToLabel:labelHeader Text:@"CURRENT JOBS" WithFont:@"opensans" FSize:11 Color:UIColorFromRGB(0xaaaaaa)];
        labelview.backgroundColor = UIColorFromRGB(0xf8f8f8);
    }
    else if (section == 1){
        
        [Helper setToLabel:labelHeader Text:@"SCHEDULED JOBS" WithFont:@"opensans" FSize:11 Color:UIColorFromRGB(0xaaaaaa)];
        labelview.backgroundColor = UIColorFromRGB(0xf8f8f8);
    }
    
    return labelview;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0) {
        if (!bookingArray.count) {
            
        }else{
            NSInteger keyStatus =[bookingArray[indexPath.row][@"status"] integerValue] ;
            switch (keyStatus) {
                case 2:
                    [self performSegueWithIdentifier:@"appointmentDetail" sender:bookingArray[indexPath.row]];
                    break;
                case 5:
                    [self performSegueWithIdentifier:@"ToMapController" sender:bookingArray[indexPath.row]];
                    break;
                case 6:
                    [self performSegueWithIdentifier:@"ToTimerVC" sender:bookingArray[indexPath.row]];
                    break;
                case 21:
                    [self performSegueWithIdentifier:@"ToTimerVC" sender:bookingArray[indexPath.row]];
                    break;
                case 22:
                    [self performSegueWithIdentifier:@"ToInvoice" sender:bookingArray[indexPath.row]];
                    break;
            }
        }
    }else{
        
        if (!laterBookingArray.count) {
        }else{
            NSInteger keyStatus =[laterBookingArray[indexPath.row][@"status"] integerValue] ;
            switch (keyStatus) {
                case 2:
                    [self performSegueWithIdentifier:@"appointmentDetail" sender:laterBookingArray[indexPath.row]];
                    break;
                case 5:
                    [self performSegueWithIdentifier:@"ToMapController" sender:laterBookingArray[indexPath.row]];
                    break;
                case 6:
                    [self performSegueWithIdentifier:@"ToTimerVC" sender:laterBookingArray[indexPath.row]];
                    break;
                case 21:
                    [self performSegueWithIdentifier:@"ToTimerVC" sender:laterBookingArray[indexPath.row]];
                    break;
                case 22:
                    [self performSegueWithIdentifier:@"ToInvoice" sender:laterBookingArray[indexPath.row]];
                    break;
            }
        }
    }
    [self.tblView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark UITableview DataSource -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
     if (laterBookingArray.count > 0 || bookingArray.count > 0)
    {
        return 2;
    }else{
        [self.view bringSubviewToFront:self.noAppoointmentLabel];
        [_noAppoointmentLabel setHidden:NO];
        return 0;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (bookingArray.count == 0 && laterBookingArray.count == 0)
    {
        [self.view bringSubviewToFront:self.noAppoointmentLabel];
        [_noAppoointmentLabel setHidden:NO];
        return 0;
    }else{
        [_noAppoointmentLabel setHidden:YES];
        switch (section) {
            case 0:
            {
                if (bookingArray.count > 0) {
                    return bookingArray.count;
                }else{
                    return  1;
                }
            }
                break;
            default:
            {
                if (laterBookingArray.count > 0) {
                    return laterBookingArray.count;
                }else{
                    return 1;
                }
            }
                break;
        }
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"HomeViewCell";
    
    HomeCell *cell = (HomeCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    if(cell == nil)
    {
        cell =[[HomeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
//    HomeCell *cell;
//    if (!cell) {
//        cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    }
    
    switch (indexPath.section) {
        case 0:
        {
            if (bookingArray.count > 0) {
                
                cell.custName.text = bookingArray[indexPath.row][@"fname"];
                cell.bookingaddress.text=bookingArray[indexPath.row][@"addrLine1"];
                cell.bookingStatus.text = bookingArray[indexPath.row][@"statusMsg"];
                NSString *distance =[NSString stringWithFormat:@"%@ KM Away",[NSNumber numberWithInteger:[bookingArray[indexPath.row][@"cat_name"]integerValue]]];
                cell.DistanceAway.text=distance;
                cell.requestType.text=[NSString stringWithFormat:@"ASAP - %@ Request",bookingArray[indexPath.row][@"cat_name"]];
                cell.bid.text=[NSString stringWithFormat:@"JOB ID:%@",bookingArray[indexPath.row][@"bid"]];
                
                [cell.noAppointments setHidden:NO];
                [cell.divider setHidden:NO];
                [cell.arrowImage setHidden:NO];
                [cell.bookingaddress setHidden:NO];
                [cell.bookingStatus setHidden:NO];
                [cell.custName setHidden:NO];
                [cell.bid setHidden:NO];
                [cell.DistanceAway setHidden:NO];
                [cell.noAppointments setHidden:YES];
                [cell.requestType setHidden:NO];

            }else{
                
                [cell.noAppointments setHidden:YES];
                cell.noAppointments.text=@"NO CURRENT BOOKINGS";
                [cell.divider setHidden:YES];
                [cell.arrowImage setHidden:YES];
                [cell.bookingaddress setHidden:YES];
                [cell.bookingStatus setHidden:YES];
                [cell.custName setHidden:YES];
                [cell.bid setHidden:YES];
                [cell.DistanceAway setHidden:YES];
                [cell.noAppointments setHidden:NO];
                [cell.requestType setHidden:YES];

            }
            return cell;
            
        }
            break;
        case 1:
        {
            if (laterBookingArray.count > 0) {
                cell.custName.text = laterBookingArray[indexPath.row][@"fname"];
                cell.bookingaddress.text=laterBookingArray[indexPath.row][@"addrLine1"];
                cell.bookingStatus.text = laterBookingArray[indexPath.row][@"statusMsg"];
                NSString *distance =[NSString stringWithFormat:@"%@ KM Away",[NSNumber numberWithInteger:[laterBookingArray[indexPath.row][@"cat_name"]integerValue]]];
                cell.DistanceAway.text=distance;
                cell.requestType.text=[NSString stringWithFormat:@"ASAP - %@ Request",laterBookingArray[indexPath.row][@"cat_name"]];
                cell.bid.text=[NSString stringWithFormat:@"JOB ID:%@",laterBookingArray[indexPath.row][@"bid"]];
                
                [cell.noAppointments setHidden:NO];
                [cell.divider setHidden:NO];
                [cell.arrowImage setHidden:NO];
                [cell.bookingaddress setHidden:NO];
                [cell.bookingStatus setHidden:NO];
                [cell.custName setHidden:NO];
                [cell.bid setHidden:NO];
                [cell.DistanceAway setHidden:NO];
                [cell.noAppointments setHidden:YES];
                [cell.requestType setHidden:NO];

              
            }else{
                
                [cell.noAppointments setHidden:YES];
                cell.noAppointments.text=@"NO SCHEDULED BOOKINGS";
                [cell.divider setHidden:YES];
                [cell.arrowImage setHidden:YES];
                [cell.bookingaddress setHidden:YES];
                [cell.bookingStatus setHidden:YES];
                [cell.custName setHidden:YES];
                [cell.bid setHidden:YES];
                [cell.DistanceAway setHidden:YES];
                [cell.noAppointments setHidden:NO];
                [cell.requestType setHidden:YES];
            }
            return cell;
        }
            break;
        default:
            return nil;
            break;
    }
   
}




- (NSDateFormatter *)DateFormatterProper
{
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    });
    return formatter;
}



- (NSDateFormatter *)DateFormatterRequired
{
    static NSDateFormatter *formatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd MMM, HH:mm a";
    });
    return formatter;
}


#pragma mark - Custom Methods -

- (CGFloat)measureHeightLabel: (UILabel *)label
{
    CGSize constrainedSize = CGSizeMake(screenSize.size.width-38 , 9999);
    
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:label.font.fontName size:label.font.pointSize], NSFontAttributeName,
                                          nil];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:flStrForObj(label.text)attributes:attributesDictionary];
    
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return  requiredHeight.size.height;
}


@end
