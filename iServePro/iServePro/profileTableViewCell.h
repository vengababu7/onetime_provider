//
//  profileTableViewCell.h
//  iServePro
//
//  Created by Rahul Sharma on 29/04/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface profileTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *detailLabel;
@end
