//
//  PaymentTableViewCell.h
//  iServePro
//
//  Created by Rahul Sharma on 14/07/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *TitleOfAmount;

@property (strong, nonatomic) IBOutlet UILabel *earningLabel;
@end
