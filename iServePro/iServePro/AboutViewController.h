//
//  AboutViewController.h
//  iServePro
//
//  Created by Rahul Sharma on 02/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;

- (IBAction)paymentLogs:(id)sender;

- (IBAction)jobsLog:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *contentScrollView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *leadingContrain;
@end
