//
//  Picker.h
//  iRush
//
//  Created by Rahul Sharma on 01/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VNHPickerDelegate <NSObject>

@optional
- (void)selectedArrayIndex:(NSInteger)index andTitle:(NSString *)title;

@end

@interface VNHPicker : UIView <UIPickerViewDataSource, UIPickerViewDelegate>

@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) UITextField *pickerTextField;
@property (strong, nonatomic) NSArray *arrayOfPicker;
@property (strong, nonatomic) id<VNHPickerDelegate>delegate;
@property (assign, nonatomic) CGRect frameOfView;

- (void)loadPickerWithArray:(NSArray *)array
                     onView:(UIView *)pickerOn
                  forTextField:(UITextField *)selectedTextField;
@end
