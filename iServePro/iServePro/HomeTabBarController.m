
//  HomeTabBarController.m
//  iServePro
//
//  Created by Rahul Sharma on 12/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.


#import "HomeTabBarController.h"
#import "ChatSocketIOClient.h"
#import "iServeSplashController.h"
#import "LocationTracker.h"
#import "ChatSocketIOClient.h"

@interface HomeTabBarController ()

@property (strong, nonatomic) UIButton *onTheJobOffTheJobButton;
@property (strong, nonatomic) NSString *status;
@property LocationTracker * locationTracker;
@property (nonatomic) NSTimer* locationUpdateTimer;
@end

@implementation HomeTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
   
   
}
-(void)viewWillAppear:(BOOL)animated{
    
       
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(buttonAction) name:@"InBooking" object:nil];
    [self offTheJobOnTheJob];
    [self onOffTheJob];
}

/**
 *  Giving th Action to The ON OFF job button
 */
-(void)offTheJobOnTheJob
{
    
    UIView *view =[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-55, 5, 220, 35)];
    _onTheJobOffTheJobButton =[[UIButton alloc]initWithFrame:CGRectMake(0,0,220,35)];
    _onTheJobOffTheJobButton.titleLabel.font =[UIFont fontWithName:@"opensans-Semibold" size:12];
    
    [_onTheJobOffTheJobButton addTarget:self
                                 action:@selector(buttonAction)
                       forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:_onTheJobOffTheJobButton];
    self.navigationItem.titleView=view;
    //  self.navigationController.navigationBar.shadowImage=[UIImage imageNamed:@"home_navigationbar_shadow.png"];
}
/**
 *  Creates The Button for on the job and off the job
 */
-(void)onOffTheJob{
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    _status =[ud valueForKey:@"IserveStatus"];
    if([_status intValue]==3)
    {
        _onTheJobOffTheJobButton.selected = NO;
        [_onTheJobOffTheJobButton setBackgroundImage:[UIImage imageNamed:@"provider_popup_on_the_job_btn_off.png"] forState:UIControlStateNormal];
        [self.onTheJobOffTheJobButton setTitle:@"GO OFF THE JOB" forState:UIControlStateNormal];
        [self.onTheJobOffTheJobButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else
    {
        [_onTheJobOffTheJobButton setBackgroundImage:[UIImage imageNamed:@"accepting_booking_off_the_job_btn_off.png"] forState:UIControlStateNormal];
        _onTheJobOffTheJobButton.selected = YES;
        [self.onTheJobOffTheJobButton setTitle:@"GO ON THE JOB" forState:UIControlStateNormal];
        [self.onTheJobOffTheJobButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    [self requestUpdateIServeStatus:_status];
}


/**
 *  button action to represent to off or on the job
 */
-(void)buttonAction{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if (_onTheJobOffTheJobButton.selected) {
        _onTheJobOffTheJobButton.selected = NO;
        [_onTheJobOffTheJobButton setBackgroundImage:[UIImage imageNamed:@"provider_popup_on_the_job_btn_off.png"] forState:UIControlStateNormal];
        _status = [NSString stringWithFormat:@"%d",KDriverStatusOnline];
        
        [self.onTheJobOffTheJobButton setTitle:@"GO OFF THE JOB" forState:UIControlStateNormal];
        [self.onTheJobOffTheJobButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        // [self.onTheJobOffTheJobButton setBackgroundColor:UIColorFromRGB(0x2598ED)];
        [ud setValue:@"3" forKey:@"IserveStatus"];
    }
    else {
        _onTheJobOffTheJobButton.selected = YES;
        [_onTheJobOffTheJobButton setBackgroundImage:[UIImage imageNamed:@"accepting_booking_off_the_job_btn_off.png"] forState:UIControlStateNormal];
        _status = [NSString stringWithFormat:@"%d",kDriverStatusOffline];
        [self.onTheJobOffTheJobButton setTitle:@"GO ON THE JOB" forState:UIControlStateNormal];
        [self.onTheJobOffTheJobButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        // [self.onTheJobOffTheJobButton setBackgroundColor:UIColorFromRGB(0x3FC380)];
        [ud setValue:@"4" forKey:@"IserveStatus"];
    }
    [self requestUpdateIServeStatus:_status];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *  Service Call for updating the driver status
 *
 *  @param statusType //statusType 3-->Active ,4-->InActive
 */
- (void)requestUpdateIServeStatus:(NSString *)statusType{
    
    
    
    NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:@"Getting Status.."];
    
    NSDictionary *parameters = @{
                                 kSMPcheckUserSessionToken: [[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken],
                                 kSMPCommonDevideId:[[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey],
                                 KSMPMasterStatus:statusType,
                                 @"ent_date_time":[Helper getCurrentDateTime]
                                 };
    
    NetworkHandler *handler = [NetworkHandler sharedInstance];
    
    [handler composeRequestWithMethod:MethodUpdateMasterStaus
                              paramas:parameters
                         onComplition:^(BOOL succeeded, NSDictionary *response) {
                             [[ProgressIndicator sharedInstance]hideProgressIndicator];
                             
                             if (succeeded) {
                                 [self getIServeResponse:response];
                                 if ([statusType  isEqual: @"3"]) {
                                     [self.onTheJobOffTheJobButton setTitle:@"GO OFF THE JOB" forState:UIControlStateNormal];
                                     [self.onTheJobOffTheJobButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                                     //                                     [self.onTheJobOffTheJobButton setBackgroundColor:UIColorFromRGB(0x2598ED)];
                                     [_onTheJobOffTheJobButton setBackgroundImage:[UIImage imageNamed:@"provider_popup_on_the_job_btn_off.png"] forState:UIControlStateNormal];
                                     [self startUpdatingLocationToServer];
                                     
                                 }else if([statusType  isEqual: @"4"]){
                                     [self.onTheJobOffTheJobButton setTitle:@"GO ON THE JOB" forState:UIControlStateNormal];
                                     [self.onTheJobOffTheJobButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                                     [_onTheJobOffTheJobButton setBackgroundImage:[UIImage imageNamed:@"accepting_booking_off_the_job_btn_off.png"] forState:UIControlStateNormal];
                                     //                                     [self.onTheJobOffTheJobButton setBackgroundColor:UIColorFromRGB(0x3FC380)];
                                     [self.locationUpdateTimer invalidate];
                                     self.locationUpdateTimer = nil;
                                     [self.locationTracker stopLocationTracking];
                                 }
                             }
                             else{
                                 if ([statusType  isEqual: @"3"]) {
                                     _onTheJobOffTheJobButton.selected = YES;
                                     [self.onTheJobOffTheJobButton setTitle:@"GO ON THE JOB" forState:UIControlStateNormal];
                                     [self.onTheJobOffTheJobButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                                     [_onTheJobOffTheJobButton setBackgroundImage:[UIImage imageNamed:@"accepting_booking_off_the_job_btn_off.png"] forState:UIControlStateNormal];
                                     //                                     [self.onTheJobOffTheJobButton setBackgroundColor:UIColorFromRGB(0x3FC380)];
                                     [self.locationUpdateTimer invalidate];
                                     self.locationUpdateTimer = nil;
                                     [self.locationTracker stopLocationTracking];
                                     [ud setValue:@"4" forKey:@"IserveStatus"];
                                 }else if([statusType  isEqual: @"4"]){
                                     _onTheJobOffTheJobButton.selected = NO;
                                     [self.onTheJobOffTheJobButton setTitle:@"GO OFF THE JOB" forState:UIControlStateNormal];
                                     [self.onTheJobOffTheJobButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                                     //                                     [self.onTheJobOffTheJobButton setBackgroundColor:UIColorFromRGB(0x2598ED)];
                                     [_onTheJobOffTheJobButton setBackgroundImage:[UIImage imageNamed:@"provider_popup_on_the_job_btn_off.png"] forState:UIControlStateNormal];
                                     [self startUpdatingLocationToServer];
                                     [ud setValue:@"3" forKey:@"IserveStatus"];
                                 }
                             }
                         }];
}

/**
 *  Responding according to the response
 *
 *  @param response describes updated status
 */
- (void)getIServeResponse:(NSDictionary *)response
{
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }else if ([[response objectForKey:@"errFlag"] intValue] == 1 && [[response objectForKey:@"errNum"] intValue] == 6) {   // session token expire
        
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        return;
    }
    else if ([[response objectForKey:@"errFlag"] intValue] == 1 && [[response objectForKey:@"errNum"] intValue] == 7) {   // session token expire
         [self performSelector:@selector(userSessionTokenExpire) withObject:nil afterDelay:1];
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        return;
    }
    else
    {
        NSDictionary *dictResponse  = response;// =[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
        }
        else
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            [Helper showAlertWithTitle:@"Message" Message:[dictResponse objectForKey:@"errMsg"]];
            
        }
    }
    
}

-(void)userSessionTokenExpire{
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:
                                @"Main" bundle:[NSBundle mainBundle]];
    iServeSplashController *splah = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:splah, nil];
}

/**
 *  update the lat longs for every 4 second to socket
 */
-(void)startUpdatingLocationToServer
{
    self.locationTracker = [LocationTracker sharedInstance];
    [self.locationTracker startLocationTracking];
    NSTimeInterval time = 4.0;
    self.locationUpdateTimer =
    [NSTimer scheduledTimerWithTimeInterval:time
                                     target:self
                                   selector:@selector(updateLocation)
                                   userInfo:nil
                                    repeats:YES];
}

/**
 *  location tracker method will called
 */
-(void)updateLocation {
    [self.locationTracker updateLocationToServer];
}



@end
