//
//  ServicesTableViewCell.h
//  MelikeyPro
//
//  Created by Rahul Sharma on 27/10/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServicesTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *serviceVal;

@property (strong, nonatomic) IBOutlet UILabel *serviceName;
@end
