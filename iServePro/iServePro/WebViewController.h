//
//  webViewController.h
//  iServePro
//
//  Created by Rahul Sharma on 19/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController<UIWebViewDelegate>
@property(nonatomic,strong) NSString *weburl;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@property(strong,nonatomic) UIButton *backButton;
@end
