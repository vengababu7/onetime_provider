//
//  InvoiceViewController.h
//  iServePro
//
//  Created by Rahul Sharma on 27/06/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignatureView.h"

@interface InvoiceViewController : UIViewController<SignatureViewDelegate>


@property (strong, nonatomic) IBOutlet UILabel *timeTakenForJob;
@property (strong, nonatomic) IBOutlet UILabel *visitFare;
@property (strong, nonatomic) IBOutlet UILabel *timeFare;
@property (strong, nonatomic) IBOutlet UITextField *materialFees;
@property (strong, nonatomic) IBOutlet UITextField *miscFees;
@property (strong, nonatomic) IBOutlet UITextField *proDiscount;
@property (strong, nonatomic) IBOutlet UILabel *subTotal;

@property (strong, nonatomic) IBOutlet UILabel *discount;
@property (strong, nonatomic) IBOutlet UILabel *totalAmt;
@property (strong, nonatomic) IBOutlet SignatureView *signView;
- (IBAction)retake:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (strong, nonatomic) IBOutlet UIView *contentScrollView;
@property (strong, nonatomic) NSString *amountPerHr;
@property(strong, nonatomic) NSMutableDictionary *dictBookingDetails;
@property(strong, nonatomic) NSMutableDictionary *invoiceDetails;
@property (strong, nonatomic) NSString *minutesCount;
@property (strong, nonatomic) IBOutlet UIButton *retake;

@property (assign, nonatomic) NSInteger subTotalAmt;
@property (assign, nonatomic) NSInteger mainTotalAmt;

@property (assign, nonatomic) NSInteger matFee;
@property (assign, nonatomic) NSInteger miscFee;
@property (assign, nonatomic) NSInteger proDisc;
@property (strong, nonatomic) IBOutlet UITextView *notes;
@property (strong, nonatomic) IBOutlet UITableView *serviceTable;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightTable;
@property (strong, nonatomic) IBOutlet UIView *sliderView;

@end
