//
//  TermsNConditions.m
//  iServePro
//
//  Created by Rahul Sharma on 19/03/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "TermsNConditions.h"
#import "WebViewController.h"


@interface TermsNConditions ()
@property(nonatomic,strong)IBOutlet UITableView *tableView;
@property(nonatomic,strong)NSArray *termsOptions;
@end

@implementation TermsNConditions


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createNavLeftButton];
    self.title = NSLocalizedString(@"Terms & Conditions", @"Terms & Conditions");
    
    _termsOptions = @[NSLocalizedString(@"Terms & Conditions", @"Terms & Conditions"),NSLocalizedString(@"Privacy Policy", @"Privacy Policy")];
}
-(void)viewWillAppear:(BOOL)animated
{
    //self.view.alpha=1.0f;
    
}


-(void)cancelButtonClicked
{
    [self dismissViewControllerAnimated:YES completion:nil];
 // [self.navigationController popViewControllerAnimated:YES];
}

-(void) createNavLeftButton
{
    
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_off"]
                     forState:UIControlStateNormal];
    [navCancelButton setImage:[UIImage imageNamed:@"current_booking_cross_icon_on"]
                     forState:UIControlStateSelected];
    
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(10.0f,0.0f,40,40)];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -14;// it was -6 in iOS 6
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"gotoWebView"])
    {
        NSIndexPath *iPath = (NSIndexPath*)sender;
        WebViewController *webView = (WebViewController*)[segue destinationViewController];
        webView.title = _termsOptions[iPath.row];
        
        if (iPath.row == 0) {
            webView.weburl = APP_TERMS_AND_CONDITIONS; //terms & conditions
        }
        else if (iPath.row == 1) {
            webView.weburl = APP_PRIVACY_POLICY; //Privacy Policy
        }
        
        
        
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark UITableViewDelegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _termsOptions.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:17];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    
    cell.textLabel.text = _termsOptions[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}

#pragma mark - UITableViewDataSource Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"gotoWebView" sender:indexPath];
    
}

@end

